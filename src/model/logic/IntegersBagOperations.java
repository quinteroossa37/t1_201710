package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {


	//CALCULAR PROMEDIO
	public double computeMean(IntegersBag bag)
	{
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}

	//DAR VALOR M�XIMO 
	public int getMax(IntegersBag bag)
	{
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if( max < value)
				{
					max = value;
				}
			}

		}
		return max;
	}

	//DAR VALOR M�NIMO 
	public int getMin(IntegersBag bag)
	{
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if(  min > value)
				{
					min = value;
				}
			}

		}
		return min;
	}

	//DAR CANTIDAD IMPARES
		public int getOdd(IntegersBag bag)
		{
			int odd;
			int cantidad=0;
			if(bag != null)
			{
				Iterator<Integer> iter = bag.getIterator();
				while(iter.hasNext())
				{
					odd = iter.next();
					if(odd%2!=0)
					{
						cantidad++;
					}
				}

			}
			return cantidad;
		}

	
	//DAR CANTIDAD PARES
	public int getEven(IntegersBag bag)
	{
		int odd;
		int cantidad=0;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				odd = iter.next();
				if(odd%2==0)
				{
					cantidad++;
				}
			}

		}
		return cantidad;
	}




}
